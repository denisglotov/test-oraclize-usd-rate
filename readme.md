Get USDETH rate in ethereum smart contract
==========================================

How to run
----------

1.  Install Metamask browser extension: https://metamask.io/.

2.  Install solidity compiler solc. You may use any convenient way to get it:
    npm | docker | apt | pacman | brew.
    http://solidity.readthedocs.io/en/develop/installing-solidity.html

3.  Clone this repo:

        git clone --recurse-submodules https://bitbucket.org/denisglotov/test-oraclize-usd-rate

4.  Build the smart contracts: `./build.sh`.

    There are lots of warnings from oraclize code. I wish they fix them soon.

5.  Browse to https://wallet.ethereum.org/. You should see your account. If
    not, check the Metamask settings.

6.  In the wallet, go to Contracts tab / Deploy new contract. Switch to the
    'Contract byte code' tab and copy-paste the code from your
    `build/UsdRate.bin` file.

    As the bin file us usually quite big, I recommend opening it in some text
    editor (notepad) and copy all (Ctrl-A Ctrl-C).

    Then press deploy. Then confirm it on Metamask pop-up.

7.  Use Metamask to get the address of your new contract. For that, click on
    the last transaction, this will take you to etherscan.com page, where you
    can see something like '[Contract
    0x3ab6bb3fac5dbd2ba3a398aab1e8f818fbeeec9b Created]'.

8.  In the wallet, go to Contracts tab / Watch contract. Enter the contract
    address from the previous step, add some name, say 'UsdRate1'. To the 'JSON
    interface' field copy-paste your `build\UsdRate.abi` file. Then click OK.
