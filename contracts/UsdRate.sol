pragma solidity ^0.4.19;
import "github.com/oraclize/ethereum-api/oraclizeAPI.sol";


contract UsdRate is usingOraclize {

    uint public rate;
    uint[] public history;
    address public owner;
    bool public paused;
    uint public intervalSeconds = 60;

    event LogPriceUpdated(string rate, bytes32 query);
    event LogNewOraclizeQuery(string description, bytes32 query, uint fee);
    event LogError(string description, uint price);

    function UsdRate() public payable {
        owner = msg.sender;
        updatePrice(0);
    }

    function __callback(bytes32 _query, string _result) public {
        require(msg.sender == oraclize_cbAddress());
        rate = parseInt(_result, 3);
        history.push(rate);
        LogPriceUpdated(_result, _query);
        updatePrice(intervalSeconds);
    }

    function updatePrice(uint _waitSeconds) public payable {
        uint fee = oraclize_getPrice("URL");
        if (paused) {
            LogError("Contract is paused. Unpause first.", fee);
        } else if (fee > this.balance) {
            LogError("Not enough balance, please add some eth to cover for the query fee", fee);
        } else {
            bytes32 query = oraclize_query(
                _waitSeconds,
                "URL",
                "json(https://api.coinmarketcap.com/v1/ticker/ethereum).[0].price_usd");
            LogNewOraclizeQuery("Oraclize query sent...", query, fee);
        }
    }

    function pause(bool _paused) public {
        paused = _paused;
    }

    function updateInterval(uint _seconds) public {
        intervalSeconds = _seconds;
    }

    function destruct() public {
        require(msg.sender == owner);
        LogError("Self destructing and returning balance", this.balance);
        selfdestruct(owner);
    }
}
